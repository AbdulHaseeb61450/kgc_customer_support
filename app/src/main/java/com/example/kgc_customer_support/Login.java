package com.example.kgc_customer_support;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.Account.CustomersListAccounts;
import com.example.kgc_customer_support.DataEntry.CustomersList;
import com.example.kgc_customer_support.Documentation.CustomerListDocumentation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends AppCompatActivity {

    EditText Username,Password;
    Button Login;
    ProgressBar Loader;
    TextView versionnumber;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    /*public static final String url = "https://squarepro.net/AccountReceipt/";*/
    public static final String url = "https://backendar.squarepro.net/AccountReceipt/";
    /*public static final String url = "http://192.168.18.121:8051/AccountReceipt/";*/
    String FCMTOKEN;
    int ALL_PERMISSIONS = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        requestPermission();

        Initialize_ids();

        GetFCMToken();

        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        if (sharedpreferences.getInt("Dashboarduserid", 0) == 0)
        {

        }
        else
        {
            if (sharedpreferences.getString("role_id", "0").equals("101"))
            {
                startActivity (new Intent(Login.this, CustomersList.class));
                finish ();
            }
            else if (sharedpreferences.getString("role_id", "0").equals("102"))
            {
                startActivity (new Intent(Login.this, CustomersListAccounts.class));
                finish ();
            }
            else
            {
                startActivity (new Intent(Login.this, CustomerListDocumentation.class));
                finish ();
            }

        }

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Username.getText().toString().trim().equals("") && !Password.getText().toString().trim().equals(""))
                {
                    boolean emailcheck = isEmailValid(Username.getText().toString().trim());
                    if (emailcheck)
                    {

                        Loader.setVisibility(View.VISIBLE);
                        String email = Username.getText().toString().trim();
                        String password = Password.getText().toString().trim();
                        Login(email,password);
                    }
                    else
                    {
                        Username.setError("Enter Correct Email");
                    }

                }
                else if (Username.getText().toString().trim().equals("") && Password.getText().toString().trim().equals(""))
                {
                    Username.setError("Enter Email");
                    Password.setError("Enter Password");
                }
                else if(Username.getText().toString().trim().equals(""))
                {
                    Username.setError("Enter Email");
                }
                else if(Password.getText().toString().trim().equals(""))
                {
                    Password.setError("Enter Password");
                }
            }
        });
    }

    //************************************************************************************************//
    private void Initialize_ids()
    {
        Username = findViewById(R.id.Email);
        Password = findViewById(R.id.Password);
        Login = findViewById(R.id.btnLogin);
        Loader = findViewById(R.id.loader);
        versionnumber = findViewById(R.id.versionnumber);

        int version = getVersionCode();
        versionnumber.setText("Version: " + version + ".0");
        Loader.setVisibility(View.GONE);
    }

    public int getVersionCode(){
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void requestPermission() {
        final String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.INTERNET,Manifest.permission.ACCESS_NETWORK_STATE};
        ActivityCompat.requestPermissions(this, permissions, ALL_PERMISSIONS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(Login.this,"permission granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Login.this,"permission not granted", Toast.LENGTH_SHORT).show();
            }
        }

    }
    //************************************************************************************************//

    //************************************************************************************************//
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-.]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    //************************************************************************************************//

    //************************************************************************************************//
    private void Login(String Email, String Password)
    {
        JSONObject param = new JSONObject();
        try {
            param.put("email", Email);
            param.put("password", Password);
            param.put("fcmtoken", FCMTOKEN);
            Log.d("Haseeb", "PARAM: " + FCMTOKEN);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestQueue queue = Volley.newRequestQueue(Login.this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "loginoperator",param,
                new com.android.volley.Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("KGCRESPONSE","Dashboarduserlogin: " + response.toString());
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                String token = "";
                                JSONArray json = response.getJSONArray("response");
                                for (int i = 0; i < json.length(); i++)
                                {
                                    JSONObject jsonObject = json.getJSONObject(i);
                                    token = jsonObject.getString("token").toString().trim();
                                }

                                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.clear();
                                editor.putString("token", token);
                                editor.commit();
                                Verify(token);
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(Login.this, "Invalid Username And Password", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","Dashboarduserlogin: " + error.toString());
                        Loader.setVisibility(View.GONE);
                        Toast.makeText(Login.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(jsonObjectRequest);
    }
    //************************************************************************************************//
    private void Verify(String Token)
    {
        RequestQueue queue = Volley.newRequestQueue(Login.this);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url + "Accountappverify",
                new com.android.volley.Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.d("KGCRESPONSE","Accountappverify: " + response.toString());
                        try {
                            JSONObject json = new JSONObject(response);
                            if (json.getString("status").equals("true") && json.getString("message").equals("Sucessfull"))
                            {
                                JSONObject responseobject = (JSONObject) json.getJSONObject("response");
                                JSONObject userobject = (JSONObject) responseobject.getJSONObject("userinfo");
                                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.clear();
                                editor.putString("role_id", userobject.getString("role_id"));
                                editor.putInt("Dashboarduserid", userobject.getInt("Dashboarduserid"));
                                editor.putString("email", userobject.getString("email"));
                                editor.putString("password", userobject.getString("password"));
                                editor.putString("name", userobject.getString("name"));
                                editor.putString("phoneNo", userobject.getString("phoneNo"));
                                editor.putString("status_id", userobject.getString("status_id"));
                                editor.putString("Description", userobject.getString("Description"));
                                editor.putString("Datetime", userobject.getString("Datetime"));
                                editor.putString("token", Token);
                                editor.commit();
                                FirebaseMessaging.getInstance().subscribeToTopic("General").addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d("Haseeb", "Subscribe Ok");
                                    }
                                });
                                if (userobject.getString("role_id").equals("101"))
                                {
                                    FirebaseMessaging.getInstance().subscribeToTopic("DataEntry").addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d("Haseeb", "Subscribe Ok");
                                        }
                                    });
                                    Loader.setVisibility(View.GONE);
                                    Intent intent = new Intent(Login.this,CustomersList.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish ();
                                }
                                else if (userobject.getString("role_id").equals("102"))
                                {
                                    FirebaseMessaging.getInstance().subscribeToTopic("Account").addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d("Haseeb", "Subscribe Ok");
                                        }
                                    });
                                    Loader.setVisibility(View.GONE);
                                    Intent intent = new Intent(Login.this,CustomersListAccounts.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish ();
                                }
                                else if (userobject.getString("role_id").equals("103"))
                                {
                                    FirebaseMessaging.getInstance().subscribeToTopic("Documentation").addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d("Haseeb", "Subscribe Ok");
                                        }
                                    });
                                    Loader.setVisibility(View.GONE);
                                    Intent intent = new Intent(Login.this,CustomerListDocumentation.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish ();
                                }
                                else
                                {
                                    Loader.setVisibility(View.GONE);
                                    Toast.makeText(Login.this, "Admin user", Toast.LENGTH_LONG).show();
                                }

                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(Login.this, "Invalid Username And Password", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new com.android.volley.Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","Dashboarduserloginveryfy: " + error.toString());
                        Loader.setVisibility(View.GONE);
                        Toast.makeText(Login.this, error.toString(), Toast.LENGTH_LONG).show();
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders()
            {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + Token);
                Log.d("KGCPARAMS","Dashboarduserloginveryfy: " + param.toString());
                return param;
            }
        };
        queue.add(postRequest);
    }

    //************************************************************************************************//

    //************************************************************************************************//
    public String GetFCMToken(){

        FirebaseApp.initializeApp(getApplicationContext());
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("Haseeb", "Fetching FCM registration token failed", task.getException());
                            return;
                        }
                        FCMTOKEN = task.getResult();
                    }
                });

        return FCMTOKEN;
    }
    //************************************************************************************************//
}