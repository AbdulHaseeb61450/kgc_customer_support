package com.example.kgc_customer_support;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Map;

public class AccToDate implements Comparator<Map<String, String>> {
    @Override
    public int compare(Map<String, String> lhs, Map<String, String> rhs) {
        // TODO Auto-generated method stub
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        try {
            int c = df.parse(rhs.get("Datetime")).compareTo(
                    df.parse(lhs.get("Datetime")));
            return c;

        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}