package com.example.kgc_customer_support.Documentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.CellIdentity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.Account.ViewDetailAccounts;
import com.example.kgc_customer_support.Account.ViewSheetAccoints;
import com.example.kgc_customer_support.R;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.os.Environment.DIRECTORY_DOCUMENTS;
import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class ViewSheetDocumentation extends AppCompatActivity {

    Button btnConfirm,btnGenerate,btnCancel;
    LinearLayout FILENO,FATHERNAME,EMAIL,PASSPORT;
    TextView RecieptNo,GeneratedDate,FileNo,Name,FatherName,ContactNo,Email,Cnic,Passport,Amount,ModeOfPayment,ModeDescription,Dated,PlotNo,PaymentType,DrawnThrough,Remarks;
    SharedPreferences sharedpreferences;
    HashMap<String,String> MAP = new HashMap<>();
    ProgressBar Loader;
    boolean CHECK = false;
    private File pdfFile;
    EditText file;
    TextView EDIT;
    int visible;
    String FILENUMBER;
    File docsFolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_sheet_documentation);

        CHECK = false;
        Initialize_ids();

        EDIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewSheetDocumentation.this, ConfirmDetails.class);
                intent.putExtra("details",MAP);
                startActivity(intent);
                finish();
            }
        });
    }

    private void Initialize_ids()
    {
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        Loader = findViewById(R.id.loader);
        Loader.setVisibility(View.VISIBLE);

        EDIT = findViewById(R.id.EDIT);
        RecieptNo = findViewById(R.id.RecieptNo);
        GeneratedDate = findViewById(R.id.GeneratedDate);
        FileNo = findViewById(R.id.FileNo);
        Name = findViewById(R.id.Name);
        FatherName = findViewById(R.id.FatherName);
        ContactNo = findViewById(R.id.ContactNo);
        Email = findViewById(R.id.Email);
        Cnic = findViewById(R.id.Cnic);
        Passport = findViewById(R.id.Passport);
        Amount = findViewById(R.id.Amount);
        ModeOfPayment = findViewById(R.id.ModeOfPayment);
        ModeDescription = findViewById(R.id.ModeDescription);
        Dated = findViewById(R.id.Dated);
        PlotNo = findViewById(R.id.PlotNo);
        PaymentType = findViewById(R.id.PaymentType);
        DrawnThrough = findViewById(R.id.DrawnThrough);
        Remarks = findViewById(R.id.Remarks);

        FILENO = findViewById(R.id.FILENO);
        FATHERNAME = findViewById(R.id.FATHERNAME);
        EMAIL= findViewById(R.id.EMAIL);
        EMAIL= findViewById(R.id.EMAIL);
        PASSPORT= findViewById(R.id.PASSPORT);

        btnConfirm = findViewById(R.id.btnConfirm);
        btnGenerate = findViewById(R.id.btnGenerate);
        btnCancel = findViewById(R.id.btnCancel);

        file = findViewById(R.id.file);

        SetData();
    }

    private void SetData()
    {
        MAP = (HashMap<String, String>) getIntent().getSerializableExtra("details");
        RecieptNo.setText(MAP.get("PaymentReceiptNo"));
        GeneratedDate.setText(MAP.get("Datetime"));
        Name.setText(MAP.get("salutation") + " " + MAP.get("ClientName"));
        ContactNo.setText(MAP.get("PhoneNumber"));
        Cnic.setText(MAP.get("CNIC"));
        Amount.setText(MAP.get("Amount") + " " + MAP.get("CurrencyTypeDescription"));
        ModeOfPayment.setText(MAP.get("PaymentthroughName"));
        ModeDescription.setText(MAP.get("PaymentThroughDescription"));
        Dated.setText(MAP.get("Dated"));
        PlotNo.setText(MAP.get("Plot_UnitNo"));
        PaymentType.setText(MAP.get("InstallmentNo"));
        DrawnThrough.setText(MAP.get("Through"));
        Remarks.setText(MAP.get("Remarks"));

        FileNo.setText(MAP.get("FileNumber"));
        FatherName.setText(MAP.get("Relation") +" "+ MAP.get("FamailyName"));
        Passport.setText(MAP.get("PassportNo"));
        Email.setText(MAP.get("EmailId"));

        String fileno = MAP.get("FileNumber");
        String fathername = MAP.get("FamailyName");
        String passport = MAP.get("PassportNo");
        String email = MAP.get("EmailId");

        if (fileno.equals("N/A") || fileno.equals(""))
        {
            FILENO.setVisibility(View.GONE);
            file.setVisibility(View.VISIBLE);
        }
        if (fathername.equals("N/A") || fathername.equals(""))
        {
            FATHERNAME.setVisibility(View.GONE);
        }
        if (passport.equals("N/A") || passport.equals(""))
        {
            PASSPORT.setVisibility(View.GONE);
        }
        if (email.equals("N/A") || email.equals(""))
        {
            EMAIL.setVisibility(View.GONE);
        }

        String BUTTONTEXT = MAP.get("status_name");
        if (BUTTONTEXT.equals("Pending") || BUTTONTEXT.equals("Reject") )
        {
            btnConfirm.setVisibility(View.GONE);
            file.setVisibility(View.GONE);
            EDIT.setVisibility(View.GONE);
        }
        else if (BUTTONTEXT.equals("Closed"))
        {
            btnConfirm.setVisibility(View.GONE);
            file.setVisibility(View.GONE);
            EDIT.setVisibility(View.GONE);
            btnGenerate.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.VISIBLE);
        }
        else if (BUTTONTEXT.equals("Cancelled"))
        {
            btnConfirm.setVisibility(View.GONE);
            file.setVisibility(View.GONE);
            EDIT.setVisibility(View.GONE);
            btnGenerate.setVisibility(View.VISIBLE);
            btnCancel.setVisibility(View.GONE);
        }
        else
        {
            btnConfirm.setVisibility(View.VISIBLE);
        }
        Loader.setVisibility(View.GONE);
    }

    public void Confirm(View view)
    {
        Loader.setVisibility(View.VISIBLE);
        visible = file.getVisibility();
        if (visible == 0)
        {
            FILENUMBER = file.getText().toString().trim();
            if (FILENUMBER.equals("") || FILENUMBER.equals("N/A"))
            {
                file.setError("Invalid File No");
                Loader.setVisibility(View.GONE);
            }
            else
            {
                UpdatePaymentFileNumber();
            }
        }
        else
        {
            UpdateStatus(105);
        }
    }

    public void CANCELRECEIPT(View view)
    {
        Intent intent = new Intent(ViewSheetDocumentation.this, Cancelreceipt_Documentation.class);
        intent.putExtra("details",MAP);
        startActivity(intent);
        finish();
    }

    public void GENERATE(View view)
    {
        visible = file.getVisibility();
        try {
            createPdf();
        } catch (FileNotFoundException e) {
            Loader.setVisibility(View.GONE);
            e.printStackTrace();
        } catch (DocumentException e) {
            Loader.setVisibility(View.GONE);
            e.printStackTrace();
        } catch (IOException e) {
            Loader.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    private void UpdateStatus(int status)
    {
        JSONObject param = new JSONObject();
        try {
            param.put("PaymentReceiptNo",Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("Confirmation_Status",status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url + "UpdatePaymentStatus", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","UpdatePaymentStatus: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                try {
                                    Notify();
                                    createPdf();
                                } catch (FileNotFoundException e) {
                                    Loader.setVisibility(View.GONE);
                                    e.printStackTrace();
                                } catch (DocumentException e) {
                                    Loader.setVisibility(View.GONE);
                                    e.printStackTrace();
                                }
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(ViewSheetDocumentation.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException | IOException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","UpdatePaymentStatus: " + error.toString());
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(ViewSheetDocumentation.this);
        requestQueue.add(jsonObjectRequest);
    }


    private void createPdf() throws IOException, DocumentException {
        Loader.setVisibility(View.VISIBLE);

        if (visible == 0)
        {
            docsFolder = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
                    + "/" + MAP.get("ClientName") + "-" + FILENUMBER);
            //docsFolder = new File(Environment.getExternalStorageDirectory() + "/" + MAP.get("ClientName") + "-" + FILENUMBER);
            if (!docsFolder.exists()) {
                docsFolder.mkdirs();
            }
        }
        else
        {
            docsFolder = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
                    + "/" + MAP.get("ClientName") + "-" + MAP.get("FileNumber"));
            //docsFolder = new File(Environment.getExternalStorageDirectory() + "/" + MAP.get("ClientName") + "-" + MAP.get("FileNumber"));
            if (!docsFolder.exists()) {
                docsFolder.mkdirs();
            }
        }


        String pdfname = MAP.get("PaymentReceiptNo") + ".pdf";
        pdfFile = new File(docsFolder.getAbsolutePath(), pdfname);
        OutputStream output = new FileOutputStream(pdfFile);
        Document document = new Document(PageSize.A4);
        PdfPTable table = new PdfPTable(new float[]{3, 3});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setFixedHeight(20);
        table.setTotalWidth(PageSize.A4.getWidth());
        table.setWidthPercentage(100);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        Toast.makeText(this, "adding data", Toast.LENGTH_SHORT).show();

        if (visible == 0)
        {
            table.addCell("File/Ref No");
            table.addCell(FILENUMBER);
        }
        else
        {
            table.addCell("File/Ref No");
            table.addCell(MAP.get("FileNumber"));
        }

        table.addCell("Reciept No");
        table.addCell(MAP.get("PaymentReceiptNo"));

        table.addCell("Date");
        table.addCell(GetDateTime());

        table.addCell("Received with gratitude from");
        table.addCell(MAP.get("ClientName"));

        table.addCell("CNIC No");
        table.addCell(MAP.get("CNIC"));

        table.addCell("Payment recieved against");
        table.addCell(MAP.get("Plot_UnitNo"));

        table.addCell("Payment Description");
        table.addCell(MAP.get("InstallmentNo"));

        table.addCell("Amount Recieved");
        table.addCell(MAP.get("Amount") + " " + MAP.get("CurrencyTypeName"));

        table.addCell("Mode of Payment");
        table.addCell(MAP.get("PaymentthroughName"));

        table.addCell("Transaction Date");
        table.addCell(MAP.get("Dated"));


        /*table.addCell("Remarks");*/
        Font white = new Font();

        if (MAP.get("Remarks").length() < 37)
        {
            table.addCell("Remarks");
            table.addCell(MAP.get("Remarks"));
        }
        else if (MAP.get("Remarks").length() > 37 && MAP.get("Remarks").length() < 111)
        {

            white.setColor(BaseColor.BLACK);
            PdfPCell pCellremarks = new PdfPCell(new Phrase("Remarks" , white));
            pCellremarks.setFixedHeight(40);
            pCellremarks.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCellremarks);

            white.setColor(BaseColor.BLACK);
            PdfPCell pCell = new PdfPCell(new Phrase(MAP.get("Remarks") , white));
            pCell.setFixedHeight(40);
            pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCell);
        }
        else if (MAP.get("Remarks").length() > 111)
        {

            white.setColor(BaseColor.BLACK);
            PdfPCell pCellremarks = new PdfPCell(new Phrase("Remarks" , white));
            pCellremarks.setFixedHeight(80);
            pCellremarks.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCellremarks);

            white.setColor(BaseColor.BLACK);
            PdfPCell pCell = new PdfPCell(new Phrase(MAP.get("Remarks") , white));
            pCell.setFixedHeight(80);
            pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCell);
        }
        else
        {
            //Do Nothing
        }

        PdfWriter.getInstance(document, output);
        document.open();
        Font f = new Font(Font.FontFamily.TIMES_ROMAN, 20.0f, Font.BOLD, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph("E-Receipt \n\n", f);
        paragraph.setAlignment(Element.ALIGN_CENTER);

        Font g = new Font(Font.FontFamily.TIMES_ROMAN, 6.0f, Font.BOLD, BaseColor.BLACK);
        Paragraph footer1 = new Paragraph("For PALM DREAMS \n\n", g);
        footer1.setAlignment(Element.ALIGN_LEFT);
        Paragraph footer2 = new Paragraph("This reciept is subject to realization of payment transfer \n\n", g);
        footer2.setAlignment(Element.ALIGN_LEFT);
        Paragraph footer3 = new Paragraph("This is a computer generated reciept therefore it does not require any signature \n\n", g);
        footer3.setAlignment(Element.ALIGN_CENTER);

        InputStream ims = getAssets().open("loginlo.png");
        Bitmap bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());


        try {
            document.add(image);
            document.add(paragraph);
            document.add(table);
            document.add(footer1);
            document.add(footer2);
            document.add(footer3);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        document.close();
        Toast.makeText(this, "generated", Toast.LENGTH_SHORT).show();
        Loader.setVisibility(View.GONE);

        CHECK = true;

        Intent intentShare = new Intent(Intent.ACTION_SEND);
        intentShare.setType("application/pdf");
        Uri screenshotUri = FileProvider.getUriForFile(ViewSheetDocumentation.this, getApplicationContext().getPackageName() + ".fileprovider", pdfFile);
        intentShare.putExtra(Intent.EXTRA_STREAM,screenshotUri);
        startActivity(Intent.createChooser(intentShare, "Share the file ..."));
    }

    private void Notify()
    {
        JSONObject param = new JSONObject();
        try {
            param.put("title", "Payment Receipt Sent");
            param.put("message", MAP.get("ClientName") + "'s payment reciept sent. Reciept No: " + Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("fcmtoken", "/topics/General");
            param.put("Agentid", sharedpreferences.getInt("Dashboarduserid", 0));
            param.put("PaymentReceiptNo", Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("Confirmation_Status", 105);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "UpdateNotify", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CurrencyType: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                Loader.setVisibility(View.GONE);
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(ViewSheetDocumentation.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CurrencyType: " + error.toString());
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(ViewSheetDocumentation.this);
        requestQueue.add(jsonObjectRequest);
    }

    private void UpdatePaymentFileNumber()
    {
        JSONObject param = new JSONObject();
        try {
            param.put("Filenumber",file.getText().toString().trim());
            param.put("PaymentReceiptNo",Integer.parseInt(MAP.get("PaymentReceiptNo")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url + "UpdatePaymentFileNumber", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CreateReceipt: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                UpdateStatus(105);
                                Loader.setVisibility(View.GONE);
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(ViewSheetDocumentation.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            Toast.makeText(ViewSheetDocumentation.this, e.toString(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CreateReceipt: " + error.toString());
                        Toast.makeText(ViewSheetDocumentation.this, error.toString(), Toast.LENGTH_LONG).show();
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(ViewSheetDocumentation.this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CHECK)
        {
            Intent intent = new Intent(ViewSheetDocumentation.this, CustomerListDocumentation.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish ();
        }
        else
        {

        }
    }

    private String GetDateTime()
    {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        return dateToStr;
    }
}