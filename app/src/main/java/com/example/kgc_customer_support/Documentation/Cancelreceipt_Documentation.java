package com.example.kgc_customer_support.Documentation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.R;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.os.Environment.DIRECTORY_DOCUMENTS;
import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class Cancelreceipt_Documentation extends AppCompatActivity {

    Button btnConfirm;
    LinearLayout FILENO,FATHERNAME,EMAIL,PASSPORT;
    TextView RecieptNo,GeneratedDate,FileNo,Name,FatherName,ContactNo,Email,Cnic,Passport,Amount,ModeOfPayment,ModeDescription,Dated,PlotNo,PaymentType,DrawnThrough;
    SharedPreferences sharedpreferences;
    HashMap<String,String> MAP = new HashMap<>();
    ProgressBar Loader;
    boolean CHECK = false;
    private File pdfFile;
    int visible;
    File docsFolder;
    EditText changedremarks;
    String remarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancelreceipt__documentation);

        CHECK = false;
        Initialize_ids();
    }

    private void Initialize_ids()
    {
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        Loader = findViewById(R.id.loader);
        Loader.setVisibility(View.VISIBLE);

        RecieptNo = findViewById(R.id.RecieptNo);
        GeneratedDate = findViewById(R.id.GeneratedDate);
        FileNo = findViewById(R.id.FileNo);
        Name = findViewById(R.id.Name);
        FatherName = findViewById(R.id.FatherName);
        ContactNo = findViewById(R.id.ContactNo);
        Email = findViewById(R.id.Email);
        Cnic = findViewById(R.id.Cnic);
        Passport = findViewById(R.id.Passport);
        Amount = findViewById(R.id.Amount);
        ModeOfPayment = findViewById(R.id.ModeOfPayment);
        ModeDescription = findViewById(R.id.ModeDescription);
        Dated = findViewById(R.id.Dated);
        PlotNo = findViewById(R.id.PlotNo);
        PaymentType = findViewById(R.id.PaymentType);
        DrawnThrough = findViewById(R.id.DrawnThrough);
        changedremarks = findViewById(R.id.changedremarks);

        FILENO = findViewById(R.id.FILENO);
        FATHERNAME = findViewById(R.id.FATHERNAME);
        EMAIL= findViewById(R.id.EMAIL);
        EMAIL= findViewById(R.id.EMAIL);
        PASSPORT= findViewById(R.id.PASSPORT);

        btnConfirm = findViewById(R.id.btnConfirm);
        SetData();
    }

    private void SetData()
    {
        MAP = (HashMap<String, String>) getIntent().getSerializableExtra("details");
        RecieptNo.setText(MAP.get("PaymentReceiptNo"));
        GeneratedDate.setText(MAP.get("Datetime"));
        Name.setText(MAP.get("salutation") + " " + MAP.get("ClientName"));
        ContactNo.setText(MAP.get("PhoneNumber"));
        Cnic.setText(MAP.get("CNIC"));
        Amount.setText(MAP.get("Amount") + " " + MAP.get("CurrencyTypeDescription"));
        ModeOfPayment.setText(MAP.get("PaymentthroughName"));
        ModeDescription.setText(MAP.get("PaymentThroughDescription"));
        Dated.setText(MAP.get("Dated"));
        PlotNo.setText(MAP.get("Plot_UnitNo"));
        PaymentType.setText(MAP.get("InstallmentNo"));
        DrawnThrough.setText(MAP.get("Through"));
        changedremarks.setText(MAP.get("Remarks").toString());

        FileNo.setText(MAP.get("FileNumber"));
        FatherName.setText(MAP.get("Relation") +" "+ MAP.get("FamailyName"));
        Passport.setText(MAP.get("PassportNo"));
        Email.setText(MAP.get("EmailId"));

        String fileno = MAP.get("FileNumber");
        String fathername = MAP.get("FamailyName");
        String passport = MAP.get("PassportNo");
        String email = MAP.get("EmailId");

        if (fileno.equals("N/A") || fileno.equals(""))
        {
            FILENO.setVisibility(View.GONE);
        }
        if (fathername.equals("N/A") || fathername.equals(""))
        {
            FATHERNAME.setVisibility(View.GONE);
        }
        if (passport.equals("N/A") || passport.equals(""))
        {
            PASSPORT.setVisibility(View.GONE);
        }
        if (email.equals("N/A") || email.equals(""))
        {
            EMAIL.setVisibility(View.GONE);
        }

        Loader.setVisibility(View.GONE);
    }

    public void Confirm(View view)
    {
        Loader.setVisibility(View.VISIBLE);
        if (changedremarks.getText().toString().trim().equals("") || changedremarks.getText().toString().trim().equals(null))
        {
            changedremarks.setError("Enter Remarks");
        }
        else
        {
            UpdateStatus(107);
        }
    }

    public void GENERATE(View view)
    {
        try {
            createPdf();
        } catch (FileNotFoundException e) {
            Loader.setVisibility(View.GONE);
            e.printStackTrace();
        } catch (DocumentException e) {
            Loader.setVisibility(View.GONE);
            e.printStackTrace();
        } catch (IOException e) {
            Loader.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    private void UpdateStatus(int status)
    {
        remarks = changedremarks.getText().toString().trim();
        JSONObject param = new JSONObject();
        try {
            param.put("PaymentReceiptNo",Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("Confirmation_Status",status);
            param.put("Remarks",remarks);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url + "CancelReceipt", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","UpdatePaymentStatus: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                try {
                                    Notify();
                                    createPdf();
                                } catch (FileNotFoundException e) {
                                    Loader.setVisibility(View.GONE);
                                    e.printStackTrace();
                                } catch (DocumentException e) {
                                    Loader.setVisibility(View.GONE);
                                    e.printStackTrace();
                                }
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(Cancelreceipt_Documentation.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException | IOException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","UpdatePaymentStatus: " + error.toString());
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Cancelreceipt_Documentation.this);
        requestQueue.add(jsonObjectRequest);
    }


    private void createPdf() throws IOException, DocumentException {
        Loader.setVisibility(View.VISIBLE);

        if (visible == 0)
        {
            docsFolder = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
                    + "/" + MAP.get("ClientName") + "-" + MAP.get("FileNumber") );
            //docsFolder = new File(Environment.getExternalStorageDirectory() + "/" + MAP.get("ClientName") + "-" + FILENUMBER);
            if (!docsFolder.exists()) {
                docsFolder.mkdirs();
            }
        }
        else
        {
            docsFolder = new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOCUMENTS).getPath()
                    + "/" + MAP.get("ClientName") + "-" + MAP.get("FileNumber"));
            //docsFolder = new File(Environment.getExternalStorageDirectory() + "/" + MAP.get("ClientName") + "-" + MAP.get("FileNumber"));
            if (!docsFolder.exists()) {
                docsFolder.mkdirs();
            }
        }


        String pdfname = "C-" + MAP.get("PaymentReceiptNo") + ".pdf";
        pdfFile = new File(docsFolder.getAbsolutePath(), pdfname);
        OutputStream output = new FileOutputStream(pdfFile);
        Document document = new Document(PageSize.A4);
        PdfPTable table = new PdfPTable(new float[]{3, 3});
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setFixedHeight(20);
        table.setTotalWidth(PageSize.A4.getWidth());
        table.setWidthPercentage(100);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        Toast.makeText(this, "adding data", Toast.LENGTH_SHORT).show();

        table.addCell("File/Ref No");
        table.addCell(MAP.get("FileNumber"));

        table.addCell("Reciept No");
        table.addCell(MAP.get("PaymentReceiptNo"));

        table.addCell("Date");
        table.addCell(GetDateTime());

        table.addCell("Received with gratitude from");
        table.addCell(MAP.get("ClientName"));

        table.addCell("CNIC No");
        table.addCell(MAP.get("CNIC"));

        table.addCell("Payment recieved against");
        table.addCell(MAP.get("Plot_UnitNo"));

        table.addCell("Payment Description");
        table.addCell(MAP.get("InstallmentNo"));

        table.addCell("Amount Recieved");
        table.addCell(MAP.get("Amount") + " " + MAP.get("CurrencyTypeName"));

        table.addCell("Mode of Payment");
        table.addCell(MAP.get("PaymentthroughName"));

        table.addCell("Transaction Date");
        table.addCell(MAP.get("Dated"));


        /*table.addCell("Remarks");*/
        Font white = new Font();

        if (remarks.length() < 37)
        {
            table.addCell("Remarks");
            table.addCell(remarks);
        }
        else if (remarks.length() > 37 && remarks.length() < 111)
        {

            white.setColor(BaseColor.BLACK);
            PdfPCell pCellremarks = new PdfPCell(new Phrase("Remarks" , white));
            pCellremarks.setFixedHeight(40);
            pCellremarks.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCellremarks);

            white.setColor(BaseColor.BLACK);
            PdfPCell pCell = new PdfPCell(new Phrase(MAP.get("Remarks") , white));
            pCell.setFixedHeight(40);
            pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCell);
        }
        else if (remarks.length() > 111)
        {

            white.setColor(BaseColor.BLACK);
            PdfPCell pCellremarks = new PdfPCell(new Phrase("Remarks" , white));
            pCellremarks.setFixedHeight(80);
            pCellremarks.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCellremarks);

            white.setColor(BaseColor.BLACK);
            PdfPCell pCell = new PdfPCell(new Phrase(MAP.get("Remarks") , white));
            pCell.setFixedHeight(80);
            pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(pCell);
        }
        else
        {
            //Do Nothing
        }

        PdfWriter.getInstance(document, output);
        document.open();
        Font f = new Font(Font.FontFamily.TIMES_ROMAN, 20.0f, Font.BOLD, BaseColor.BLACK);
        Paragraph paragraph = new Paragraph("E-Receipt \n\n", f);
        paragraph.setAlignment(Element.ALIGN_CENTER);

        Font g = new Font(Font.FontFamily.TIMES_ROMAN, 6.0f, Font.BOLD, BaseColor.BLACK);
        Paragraph footer1 = new Paragraph("For PALM DREAMS \n\n", g);
        footer1.setAlignment(Element.ALIGN_LEFT);
        Paragraph footer2 = new Paragraph("This reciept is subject to realization of payment transfer \n\n", g);
        footer2.setAlignment(Element.ALIGN_LEFT);
        Paragraph footer3 = new Paragraph("This is a computer generated reciept therefore it does not require any signature \n\n", g);
        footer3.setAlignment(Element.ALIGN_CENTER);

        InputStream ims = getAssets().open("loginlo.png");
        Bitmap bmp = BitmapFactory.decodeStream(ims);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Image image = Image.getInstance(stream.toByteArray());


        try {
            document.add(image);
            document.add(paragraph);
            document.add(table);
            document.add(footer1);
            document.add(footer2);
            document.add(footer3);
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        document.close();
        Toast.makeText(this, "generated", Toast.LENGTH_SHORT).show();
        Loader.setVisibility(View.GONE);

        CHECK = true;

        Intent intent = new Intent(Cancelreceipt_Documentation.this, CustomerListDocumentation.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish ();

    }

    private void Notify()
    {
        JSONObject param = new JSONObject();
        try {
            param.put("title", "Payment Receipt Cancelled");
            param.put("message", MAP.get("ClientName") + "'s payment reciept cancelled. Reciept No: " + Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("fcmtoken", "/topics/General");
            param.put("Agentid", sharedpreferences.getInt("Dashboarduserid", 0));
            param.put("PaymentReceiptNo", Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("Confirmation_Status", 107);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "UpdateNotify", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CurrencyType: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                Loader.setVisibility(View.GONE);
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(Cancelreceipt_Documentation.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CurrencyType: " + error.toString());
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(Cancelreceipt_Documentation.this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (CHECK)
        {
            Intent intent = new Intent(Cancelreceipt_Documentation.this, CustomerListDocumentation.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish ();
        }
        else
        {

        }
    }

    private String GetDateTime()
    {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        return dateToStr;
    }
}