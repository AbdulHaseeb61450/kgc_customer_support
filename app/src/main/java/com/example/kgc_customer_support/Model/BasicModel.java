package com.example.kgc_customer_support.Model;

public class BasicModel {
    int id;
    String description;

    public BasicModel() {
    }

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    @Override
    public String toString () {
        return description;
    }}
