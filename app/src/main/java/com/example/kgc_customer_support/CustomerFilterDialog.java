package com.example.kgc_customer_support;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

public class CustomerFilterDialog extends AppCompatDialogFragment {

    private FilterContactDialogListener listener;
    public LinearLayout first,second,third,fourth,fifth,sixth;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.customerfilter_dialog, null);

        builder.setView(view);

        first = view.findViewById(R.id.first);
        second = view.findViewById(R.id.second);
        third = view.findViewById(R.id.third);
        fourth = view.findViewById(R.id.fourth);
        fifth = view.findViewById(R.id.fifth);
        sixth = view.findViewById(R.id.sixth);

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactfilter("100", "All");
                dismiss();

            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactfilter("104", "Approved");
                dismiss();
            }
        });

        third.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactfilter("103", "Pending");
                dismiss();
            }
        });

        fourth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactfilter("106","Reject");
                dismiss();
            }
        });

        fifth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactfilter("105", "Closed");
                dismiss();
            }
        });

        sixth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.contactfilter("107", "Cancelled");
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (FilterContactDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    e.toString() +
                    "must implement ExampleDialogListener");
        }
    }

    public interface FilterContactDialogListener {
        void contactfilter(String code, String Description);
    }
}
