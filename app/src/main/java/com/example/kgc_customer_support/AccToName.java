package com.example.kgc_customer_support;

import java.util.Comparator;
import java.util.Map;

public class AccToName implements Comparator<Map<String, String>> {
    @Override
    public int compare(Map<String, String> lhs, Map<String, String> rhs) {
        return lhs.get("ClientName").compareTo(
                rhs.get("ClientName"));
    }
}