package com.example.kgc_customer_support.DataEntry;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.AccToName;
import com.example.kgc_customer_support.CustomerFilterDialog;
import com.example.kgc_customer_support.Documentation.CustomerHistory;
import com.example.kgc_customer_support.Documentation.CustomerListDocumentation;
import com.example.kgc_customer_support.Login;
import com.example.kgc_customer_support.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class CustomersList extends AppCompatActivity implements CustomerFilterDialog.FilterContactDialogListener{

    public boolean isonline = false;

    FloatingActionButton Add;
    ProgressBar Loader;
    public ArrayList<HashMap<String, String>> customers = new ArrayList<>();
    public ArrayList<HashMap<String, String>> filtercustomers = new ArrayList<>();
    public ArrayList<HashMap<String, String>> Backup = new ArrayList<>();
    public ListView customerlistview;
    public SimpleAdapter adapter;
    TextView filtertext;
    LinearLayout filterlayout;
    String filtercustomer;
    ImageView logout;
    SharedPreferences sharedpreferences;


    public EditText searchbox;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers_list);


        Initialize_ids();

        isonline = isOnline();
        if (isonline)
        {
            GetAllCustomers();
        }
        else
        {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                alertDialog.setTitle("KGC PROPERTIES");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(R.drawable.loginlogo);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.d("Haseeb", "Show Dialog: " + e.getMessage());
            }
        }


        customerlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                isonline = isOnline();
                if (isonline)
                {
                    Intent intent = new Intent(CustomersList.this, CustomerHistoryDataEntry.class);
                    HashMap<String,String> map = (HashMap<String,String>)customerlistview.getItemAtPosition(position);
                    intent.putExtra("ClientId",Integer.parseInt(map.get("ClientId")));
                    intent.putExtra("details",map);
                    startActivity(intent);
                }
                else
                {
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                        alertDialog.setTitle("KGC PROPERTIES");
                        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                        alertDialog.setIcon(R.drawable.loginlogo);
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        alertDialog.show();
                    } catch (Exception e) {
                        Log.d("Haseeb", "Show Dialog: " + e.getMessage());
                    }
                }
            }
        });

        filterlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDialog();
            }
        });


        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isonline = isOnline();
                if (isonline)
                {
                    CustomersList.this.startActivity (new Intent(CustomersList.this, NewCustomer.class));
                }
                else
                {
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                        alertDialog.setTitle("KGC PROPERTIES");
                        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                        alertDialog.setIcon(R.drawable.loginlogo);
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        alertDialog.show();
                    } catch (Exception e) {
                        Log.d("Haseeb", "Show Dialog: " + e.getMessage());
                    }
                }

            }
        });

        searchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseMessaging.getInstance().unsubscribeFromTopic("General").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Haseeb", "UnSubscribe Ok");
                    }
                });
                FirebaseMessaging.getInstance().unsubscribeFromTopic("DataEntry").addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("Haseeb", "UnSubscribe Ok");
                    }
                });
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.clear();
                editor.commit();
                Intent intent = new Intent(CustomersList.this,Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish ();
            }
        });


    }

    private void Initialize_ids()
    {
        searchbox = findViewById(R.id.searchbox);
        Add = findViewById(R.id.Add);
        logout = findViewById(R.id.logout);
        Loader = findViewById(R.id.loader);
        customerlistview = findViewById(R.id.customerlistview);
        filtertext = findViewById(R.id.filtertext);
        filterlayout = findViewById(R.id.filterlayout);
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);

        Loader.setVisibility(View.VISIBLE);
    }

    private void GetAllCustomers()
    {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url + "GetAllClients",null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("KGCRESPONSE","showallleads: " + response.toString());
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                customers.clear();
                                Backup.clear();
                                String PassportNo,PhoneNumber;

                                JSONArray responsearray = new JSONArray();
                                responsearray = response.getJSONArray("response");
                                for (int i = 0; i < responsearray.length(); i++)
                                {
                                    JSONObject contactobject = (JSONObject) responsearray.get(i);
                                    String salutation = (String) contactobject.get("salutation");
                                    String ClientName = (String) contactobject.get("ClientName");
                                    String Relation = (String) contactobject.get("Relation");
                                    String FamailyName = (String) contactobject.get("FamailyName");
                                    String CNIC = (String) contactobject.get("CNIC");
                                    try {
                                        PassportNo = (String) contactobject.getString("PassportNo");
                                    } catch (JSONException e) {
                                        PassportNo = "";
                                    }
                                    try {
                                        PhoneNumber = (String) contactobject.getString("PhoneNumber");
                                    } catch (JSONException e) {
                                        PhoneNumber = "";
                                    }

                                    String EmailId = (String) contactobject.get("EmailId");

                                    String Datetime = (String) contactobject.get("Datetime");
                                    String CurrencyTypeDescription = (String) contactobject.get("CurrencyTypeDescription");
                                    String Dated = (String) contactobject.get("Dated");
                                    String Plot_UnitNo = (String) contactobject.get("Plot_UnitNo");
                                    String InstallmentNo = (String) contactobject.get("InstallmentNo");
                                    String Through = (String) contactobject.get("Through");
                                    String CurrencyTypeName = (String) contactobject.get("CurrencyTypeName");
                                    String PaymentthroughName = (String) contactobject.get("PaymentthroughName");
                                    String PaymentTypeName = (String) contactobject.get("PaymentTypeName");
                                    String status_name = (String) contactobject.get("status_name");
                                    String FileNumber = (String) contactobject.get("FileNumber");
                                    int PaymentReceiptNo = (int) contactobject.getInt("PaymentReceiptNo");
                                    int ClientId = (int) contactobject.getInt("ClientId");
                                    int CurrencyTypeId = (int) contactobject.getInt("CurrencyTypeId");
                                    int Amount = (int) contactobject.getInt("Amount");
                                    int PaymentThroughId = (int) contactobject.getInt("PaymentThroughId");
                                    int PaymentTypeid = (int) contactobject.getInt("PaymentTypeid");
                                    int Agentid = (int) contactobject.getInt("Agentid");
                                    int Status_Id = (int) contactobject.getInt("Status_Id");
                                    //int Confirmation_Status = (int) contactobject.getInt("status_name");

                                    HashMap<String, String> Detail = new HashMap<>();
                                    Detail.put("salutation", salutation);
                                    Detail.put("ClientName", ClientName);
                                    Detail.put("Relation", Relation);
                                    Detail.put("FamailyName", FamailyName);
                                    Detail.put("CNIC", CNIC);
                                    Detail.put("PassportNo", PassportNo);
                                    Detail.put("EmailId", EmailId);
                                    Detail.put("PhoneNumber", PhoneNumber);
                                    Detail.put("Datetime", Datetime.substring(0,10));
                                    Detail.put("CurrencyTypeDescription", CurrencyTypeDescription);
                                    Detail.put("Dated", Dated.substring(0,9));
                                    Detail.put("Plot_UnitNo", Plot_UnitNo);
                                    Detail.put("InstallmentNo", InstallmentNo);
                                    Detail.put("Through", Through);
                                    Detail.put("CurrencyTypeName", CurrencyTypeName);
                                    Detail.put("PaymentthroughName", PaymentthroughName);
                                    Detail.put("PaymentTypeName", PaymentTypeName);
                                    Detail.put("status_name", status_name);
                                    Detail.put("FileNumber", FileNumber);

                                    Detail.put("PaymentReceiptNo", String.valueOf(PaymentReceiptNo));
                                    Detail.put("ClientId", String.valueOf(ClientId));
                                    Detail.put("CurrencyTypeId", String.valueOf(CurrencyTypeId));
                                    Detail.put("Amount", String.valueOf(Amount));
                                    Detail.put("PaymentThroughId", String.valueOf(PaymentThroughId));
                                    Detail.put("PaymentTypeid", String.valueOf(PaymentTypeid));
                                    Detail.put("Agentid", String.valueOf(Agentid));
                                    Detail.put("Status_Id", String.valueOf(Status_Id));
                                    //Detail.put("Confirmation_Status", String.valueOf(Confirmation_Status));

                                    customers.add(Detail);
                                }

                                Collections.sort(customers,new AccToName());
                                Backup.addAll(customers);

                                adapter = new SimpleAdapter(
                                        CustomersList.this, customers,
                                        R.layout.customer_list_item, new String[]{"ClientName","CNIC"}, new int[]{
                                        R.id.Name,R.id.CNIC});
                                customerlistview.setAdapter(adapter);
                                Loader.setVisibility(View.GONE);

                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(CustomersList.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Loader.setVisibility(View.GONE);
                        Log.d("KGCERROR","showallleads: " + error.toString());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(CustomersList.this);
        requestQueue.add(jsonObjectRequest);
    }

    public void filter(String charText) {
        if (charText.equals(null)) { return; }
        filtercustomers.clear();
        charText = charText.toLowerCase(Locale.getDefault());
        for (HashMap hm : Backup) {
            if (((String)hm.get("ClientName").toString().toLowerCase()).contains(charText) || ((String)hm.get("CNIC").toString()).contains(charText)) {
                filtercustomers.add(hm);
            }
        }

        Collections.sort(filtercustomers,new AccToName());
        adapter = new SimpleAdapter(
                CustomersList.this, filtercustomers,
                R.layout.customer_list_item, new String[]{"ClientName","CNIC"}, new int[]{
                R.id.Name,R.id.CNIC});
        customerlistview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        Loader.setVisibility(View.GONE);
    }

    @Override
    public void contactfilter(String code, String Description) {
        filtercustomer = code;
        filtertext.setText(Description);
        isonline = isOnline();
        if (isonline)
        {
            GetCustomersAccToStatus(filtercustomer);
        }
        else
        {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                alertDialog.setTitle("KGC PROPERTIES");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(R.drawable.loginlogo);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.d("Haseeb", "Show Dialog: " + e.getMessage());
            }
        }

    }

    public void FilterDialog() {
        CustomerFilterDialog exampleDialog1 = new CustomerFilterDialog();
        exampleDialog1.show(getSupportFragmentManager(),"Filter Contacts");
    }

    private void GetCustomersAccToStatus(String code)
    {
        int CODE = Integer.parseInt(code);
        JSONObject param = new JSONObject();
        try {
            param.put("Confirmation_Status",CODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "GetPendingPaymentReceiptClients",param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("KGCRESPONSE","showallleads: " + response.toString());
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                customers.clear();
                                Backup.clear();

                                JSONArray responsearray = new JSONArray();
                                responsearray = response.getJSONArray("response");
                                for (int i = 0; i < responsearray.length(); i++)
                                {
                                    JSONObject contactobject = (JSONObject) responsearray.get(i);
                                    String ClientId = String.valueOf(contactobject.get("ClientId"));
                                    String ClientName = (String) contactobject.get("ClientName");
                                    String CNIC = (String) contactobject.get("CNIC");

                                    HashMap<String, String> Detail = new HashMap<>();
                                    Detail.put("ClientName", ClientName);
                                    Detail.put("ClientId", ClientId);
                                    Detail.put("CNIC", CNIC);

                                    customers.add(Detail);
                                }

                                Collections.sort(customers,new AccToName());
                                Backup.addAll(customers);

                                adapter = new SimpleAdapter(
                                        CustomersList.this, customers,
                                        R.layout.customer_list_item, new String[]{"ClientName","CNIC"}, new int[]{
                                        R.id.Name,R.id.CNIC});
                                customerlistview.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                Loader.setVisibility(View.GONE);

                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(CustomersList.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Loader.setVisibility(View.GONE);
                        Log.d("KGCERROR","showallleads: " + error.toString());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(CustomersList.this);
        requestQueue.add(jsonObjectRequest);
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }
}