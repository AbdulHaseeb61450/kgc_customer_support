package com.example.kgc_customer_support.DataEntry;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.Account.CustomerAccountsHistory;
import com.example.kgc_customer_support.Account.ViewDetailAccounts;
import com.example.kgc_customer_support.Account.ViewSheetAccoints;
import com.example.kgc_customer_support.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class ViewSheetDataEntry extends AppCompatActivity {

    public boolean isonline = false;

    LinearLayout FILENO,FATHERNAME,EMAIL,PASSPORT;
    TextView RecieptNo,GeneratedDate,FileNo,Name,FatherName,ContactNo,Email,Cnic,Passport,Amount,ModeOfPayment,ModeDescription,Dated,PlotNo,PaymentType,DrawnThrough,Remarks;
    SharedPreferences sharedpreferences;
    HashMap<String,String> MAP = new HashMap<>();
    ProgressBar Loader;
    TextView EDIT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_sheet_data_entry);

        Initialize_ids();

        EDIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isonline = isOnline();
                if (isonline)
                {
                    Intent intent = new Intent(ViewSheetDataEntry.this, EditRecieptData.class);
                    intent.putExtra("details",MAP);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                        alertDialog.setTitle("KGC PROPERTIES");
                        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                        alertDialog.setIcon(R.drawable.loginlogo);
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        alertDialog.show();
                    } catch (Exception e) {
                        Log.d("Haseeb", "Show Dialog: " + e.getMessage());
                    }
                }

            }
        });
    }

    private void Initialize_ids()
    {
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        Loader = findViewById(R.id.loader);
        Loader.setVisibility(View.VISIBLE);

        RecieptNo = findViewById(R.id.RecieptNo);
        GeneratedDate = findViewById(R.id.GeneratedDate);
        FileNo = findViewById(R.id.FileNo);
        Name = findViewById(R.id.Name);
        FatherName = findViewById(R.id.FatherName);
        ContactNo = findViewById(R.id.ContactNo);
        Email = findViewById(R.id.Email);
        Cnic = findViewById(R.id.Cnic);
        Passport = findViewById(R.id.Passport);
        Amount = findViewById(R.id.Amount);
        ModeOfPayment = findViewById(R.id.ModeOfPayment);
        ModeDescription = findViewById(R.id.ModeDescription);
        Dated = findViewById(R.id.Dated);
        PlotNo = findViewById(R.id.PlotNo);
        PaymentType = findViewById(R.id.PaymentType);
        DrawnThrough = findViewById(R.id.DrawnThrough);
        Remarks = findViewById(R.id.Remarks);

        FILENO = findViewById(R.id.FILENO);
        FATHERNAME = findViewById(R.id.FATHERNAME);
        EMAIL= findViewById(R.id.EMAIL);
        PASSPORT= findViewById(R.id.PASSPORT);

        EDIT = findViewById(R.id.EDIT);

        isonline = isOnline();
        if (isonline)
        {
            SetData();
        }
        else
        {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                alertDialog.setTitle("KGC PROPERTIES");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(R.drawable.loginlogo);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.d("Haseeb", "Show Dialog: " + e.getMessage());
            }
        }
    }

    private void SetData()
    {
        MAP = (HashMap<String, String>) getIntent().getSerializableExtra("details");
        RecieptNo.setText(MAP.get("PaymentReceiptNo"));
        GeneratedDate.setText(MAP.get("Datetime"));
        Name.setText(MAP.get("salutation") + " " + MAP.get("ClientName"));
        ContactNo.setText(MAP.get("PhoneNumber"));
        Cnic.setText(MAP.get("CNIC"));
        Amount.setText(MAP.get("Amount") + " " + MAP.get("CurrencyTypeDescription"));
        ModeOfPayment.setText(MAP.get("PaymentthroughName"));
        ModeDescription.setText(MAP.get("PaymentThroughDescription"));
        Dated.setText(MAP.get("Dated"));
        PlotNo.setText(MAP.get("Plot_UnitNo"));
        PaymentType.setText(MAP.get("InstallmentNo"));
        DrawnThrough.setText(MAP.get("Through"));
        Remarks.setText(MAP.get("Remarks"));

        FileNo.setText(MAP.get("FileNumber"));
        FatherName.setText(MAP.get("Relation") +" "+ MAP.get("FamailyName"));
        Passport.setText(MAP.get("PassportNo"));
        Email.setText(MAP.get("EmailId"));

        String fileno = MAP.get("FileNumber");
        String fathername = MAP.get("FamailyName");
        String passport = MAP.get("PassportNo");
        String email = MAP.get("EmailId");

        if (fileno.equals("N/A") || fileno.equals(""))
        {
            FILENO.setVisibility(View.GONE);
        }
        if (fathername.equals("N/A") || fathername.equals(""))
        {
            FATHERNAME.setVisibility(View.GONE);
        }
        if (passport.equals("N/A") || passport.equals(""))
        {
            PASSPORT.setVisibility(View.GONE);
        }
        if (email.equals("N/A") || email.equals(""))
        {
            EMAIL.setVisibility(View.GONE);
        }

        String BUTTONTEXT = MAP.get("status_name");
        if (BUTTONTEXT.equals("Closed") || BUTTONTEXT.equals("Approved"))
        {
            EDIT.setVisibility(View.GONE);
            Loader.setVisibility(View.GONE);
        }
        else if (BUTTONTEXT.equals("Reject"))
        {
            EDIT.setVisibility(View.VISIBLE);
            Loader.setVisibility(View.GONE);
        }
        else
        {
            EDIT.setVisibility(View.VISIBLE);
            Loader.setVisibility(View.GONE);
        }

        Loader.setVisibility(View.GONE);
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }

}