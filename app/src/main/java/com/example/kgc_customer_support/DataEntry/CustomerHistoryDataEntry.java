package com.example.kgc_customer_support.DataEntry;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.AccToDate;
import com.example.kgc_customer_support.AccToName;
import com.example.kgc_customer_support.Account.CustomerAccountsHistory;
import com.example.kgc_customer_support.Account.ViewSheetAccoints;
import com.example.kgc_customer_support.CustomerFilterDialog;
import com.example.kgc_customer_support.Documentation.CustomerHistory;
import com.example.kgc_customer_support.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class CustomerHistoryDataEntry extends AppCompatActivity implements CustomerFilterDialog.FilterContactDialogListener {

    public boolean isonline = false;

    public ArrayList<HashMap<String, String>> history = new ArrayList<>();
    public ArrayList<HashMap<String, String>> filterhistory = new ArrayList<>();
    public ArrayList<HashMap<String, String>> Backup = new ArrayList<>();
    public ListView dataentryhistory;
    public SimpleAdapter adapter;
    ProgressBar Loader;
    SharedPreferences sharedpreferences;
    public EditText searchbox;
    FloatingActionButton Add;

    public LinearLayout filterlayout;
    public TextView filtertext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_history_data_entry);

        Initialize_ids();

        dataentryhistory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Intent intent = new Intent(CustomerHistoryDataEntry.this, ViewSheetDataEntry.class);
                HashMap<String,String> map = (HashMap<String,String>)dataentryhistory.getItemAtPosition(position);
                intent.putExtra("details",map);
                startActivity(intent);
                finish();

            }
        });

        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isonline = isOnline();
                if (isonline)
                {
                    Intent intent = new Intent(CustomerHistoryDataEntry.this, NewCustomer.class);
                    HashMap<String,String> map = (HashMap<String,String>)dataentryhistory.getItemAtPosition(0);
                    intent.putExtra("details",map);
                    startActivity(intent);
                }
                else
                {
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                        alertDialog.setTitle("KGC PROPERTIES");
                        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                        alertDialog.setIcon(R.drawable.loginlogo);
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        alertDialog.show();
                    } catch (Exception e) {
                        Log.d("Haseeb", "Show Dialog: " + e.getMessage());
                    }
                }
            }
        });

        filterlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterDialog();
            }
        });

        searchbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void Initialize_ids()
    {
        searchbox = findViewById(R.id.searchbox);
        Loader = findViewById(R.id.loader);
        Loader.setVisibility(View.VISIBLE);
        dataentryhistory = findViewById(R.id.dataentryhistory);
        Add = findViewById(R.id.Add);
        filterlayout = findViewById(R.id.filterlayout);
        filtertext = findViewById(R.id.filtertext);
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);

        isonline = isOnline();
        if (isonline)
        {
            GetCustomerHistory();
        }
        else
        {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                alertDialog.setTitle("KGC PROPERTIES");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(R.drawable.loginlogo);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.d("Haseeb", "Show Dialog: " + e.getMessage());
            }
        }


    }

    private void GetCustomerHistory()
    {

        Loader.setVisibility(View.VISIBLE);
        int clientid = getIntent().getIntExtra("ClientId",0);
        JSONObject param = new JSONObject();
        try {
            param.put("ClientId",clientid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "Clienthistory",param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.d("KGCRESPONSE","Clienthistory: " + response.toString());
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                history.clear();
                                filterhistory.clear();
                                Backup.clear();
                                String PassportNo,PhoneNumber;

                                JSONArray responsearray = new JSONArray();
                                responsearray = response.getJSONArray("response");
                                for (int i = 0; i < responsearray.length(); i++)
                                {
                                    JSONObject contactobject = (JSONObject) responsearray.get(i);
                                    String salutation = (String) contactobject.get("salutation");
                                    String ClientName = (String) contactobject.get("ClientName");
                                    try {
                                        PassportNo = (String) contactobject.getString("PassportNo");
                                    } catch (JSONException e) {
                                        PassportNo = "";
                                    }
                                    String EmailId = (String) contactobject.get("EmailId");
                                    PhoneNumber = (String) contactobject.getString("PhoneNumber");
                                    String CNIC = (String) contactobject.get("CNIC");
                                    String Relation = (String) contactobject.get("Relation");
                                    String FamailyName = (String) contactobject.get("FamailyName");
                                    String Datetime = (String) contactobject.get("Datetime").toString().substring(0,10);
                                    String CurrencyTypeDescription = (String) contactobject.get("CurrencyTypeDescription");
                                    String Dated = (String) contactobject.get("Dated").toString().substring(0,10);
                                    String Plot_UnitNo = (String) contactobject.get("Plot_UnitNo");
                                    String InstallmentNo = (String) contactobject.get("InstallmentNo");
                                    String Through = (String) contactobject.get("Through");
                                    String CurrencyTypeName = (String) contactobject.get("CurrencyTypeName");
                                    String PaymentThroughName = (String) contactobject.get("PaymentthroughName");
                                    String PaymentThroughDescription = (String) contactobject.get("PaymentThroughDescription");
                                    String PaymentTypeName = (String) contactobject.get("PaymentTypeName");
                                    String status_name = (String) contactobject.get("status_name");
                                    String Remarks = (String) contactobject.get("Remarks");
                                    String FileNumber = (String) contactobject.get("FileNumber");
                                    int PaymentReceiptNo = (int) contactobject.getInt("PaymentReceiptNo");
                                    int ClientId = (int) contactobject.getInt("ClientId");
                                    int CurrencyTypeId = (int) contactobject.getInt("CurrencyTypeId");
                                    int Amount = (int) contactobject.getInt("Amount");
                                    int PaymentThroughId = (int) contactobject.getInt("PaymentThroughId");
                                    int PaymentTypeid = (int) contactobject.getInt("PaymentTypeid");
                                    int Agentid = (int) contactobject.getInt("Agentid");
                                    int Status_Id = (int) contactobject.getInt("Status_Id");
                                    //int Confirmation_Status = (int) contactobject.getInt("Confirmation_Status");

                                    HashMap<String, String> Detail = new HashMap<>();
                                    Detail.put("PaymentThroughDescription", PaymentThroughDescription);
                                    Detail.put("salutation", salutation);
                                    Detail.put("ClientName", ClientName);
                                    Detail.put("Relation", Relation);
                                    Detail.put("FamailyName", FamailyName);
                                    Detail.put("CNIC", CNIC);
                                    Detail.put("PassportNo", PassportNo);
                                    Detail.put("EmailId", EmailId);
                                    Detail.put("PhoneNumber", PhoneNumber);
                                    Detail.put("Datetime", Datetime);
                                    Detail.put("CurrencyTypeDescription", CurrencyTypeDescription);
                                    Detail.put("Dated", Dated);
                                    Detail.put("Plot_UnitNo", Plot_UnitNo);
                                    Detail.put("InstallmentNo", InstallmentNo);
                                    Detail.put("Through", Through);
                                    Detail.put("CurrencyTypeName", CurrencyTypeName);
                                    Detail.put("PaymentthroughName", PaymentThroughName);
                                    Detail.put("PaymentTypeName", PaymentTypeName);
                                    Detail.put("status_name", status_name);
                                    Detail.put("FileNumber", FileNumber);
                                    Detail.put("PaymentReceiptNo", String.valueOf(PaymentReceiptNo));
                                    Detail.put("ClientId", String.valueOf(ClientId));
                                    Detail.put("CurrencyTypeId", String.valueOf(CurrencyTypeId));
                                    Detail.put("Amount", String.valueOf(Amount));
                                    Detail.put("PaymentThroughId", String.valueOf(PaymentThroughId));
                                    Detail.put("PaymentTypeid", String.valueOf(PaymentTypeid));
                                    Detail.put("Agentid", String.valueOf(Agentid));
                                    Detail.put("Status_Id", String.valueOf(Status_Id));
                                    Detail.put("Remarks", Remarks);
                                    //Detail.put("Confirmation_Status", String.valueOf(Confirmation_Status));

                                    history.add(Detail);
                                }

                                Backup.addAll(history);

                                Collections.sort(history,new AccToDate());
                                adapter = new SimpleAdapter(
                                        CustomerHistoryDataEntry.this, history,
                                        R.layout.accountshistory_list, new String[]{"ClientName","PaymentReceiptNo","status_name","Datetime"}, new int[]{
                                        R.id.name,R.id.reciept,R.id.status,R.id.date});
                                dataentryhistory.setAdapter(adapter);
                                Loader.setVisibility(View.GONE);

                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(CustomerHistoryDataEntry.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Loader.setVisibility(View.GONE);
                        Log.d("KGCERROR","Clienthistory: " + error.toString());
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(CustomerHistoryDataEntry.this);
        requestQueue.add(jsonObjectRequest);
    }

    public void filter(String charText) {
        if (charText.equals(null)) { return; }
        filterhistory.clear();
        charText = charText.toLowerCase(Locale.getDefault());
        for (HashMap hm : Backup) {
            if (((String)hm.get("status_name").toString().toLowerCase()).contains(charText) || ((String)hm.get("Datetime").toString().toLowerCase()).contains(charText)) {
                filterhistory.add(hm);
            }
        }

        Collections.sort(filterhistory,new AccToName());
        adapter = new SimpleAdapter(
                CustomerHistoryDataEntry.this, filterhistory,
                R.layout.accountshistory_list, new String[]{"ClientName","PaymentReceiptNo","status_name","Datetime"}, new int[]{
                R.id.name,R.id.reciept,R.id.status,R.id.date});
        dataentryhistory.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        Loader.setVisibility(View.GONE);
    }

    @Override
    public void contactfilter(String code, String Description) {
        filtertext.setText(Description);
        if(Description.equals("All"))
        {
            Loader.setVisibility(View.VISIBLE);
            Collections.sort(history,new AccToName());
            adapter = new SimpleAdapter(
                    CustomerHistoryDataEntry.this, history,
                    R.layout.accountshistory_list, new String[]{"ClientName","PaymentReceiptNo","status_name","Datetime"}, new int[]{
                    R.id.name,R.id.reciept,R.id.status,R.id.date});
            dataentryhistory.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Loader.setVisibility(View.GONE);
        }
        else
        {
            filter(Description);
        }

    }

    public void FilterDialog() {
        CustomerFilterDialog exampleDialog1 = new CustomerFilterDialog();
        exampleDialog1.show(getSupportFragmentManager(),"Filter Contacts");
    }

    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }
}