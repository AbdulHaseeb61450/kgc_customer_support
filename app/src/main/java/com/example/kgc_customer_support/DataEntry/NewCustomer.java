package com.example.kgc_customer_support.DataEntry;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.Model.BasicModel;
import com.example.kgc_customer_support.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class NewCustomer extends AppCompatActivity {

    ProgressBar Loader;

    public boolean isonline = false;

    EditText Name,ParentName,Phone,Email,Cnic,Passport,CurrencyText,amount,TypeOfPayment,dated,plot,installment,Through,remarks,file;
    String name,parentname,email,currencttext,Dated,Plot,through,Remarks,phone,cnic,passport,Amount,typeofpayment,Installment,SalutationDescription,RelationDescription,fileno;
    int currencyid,modeid,typeid,id;

    Spinner salutation,parent,Currency,Mode,type;
    Button btnSave;
    SharedPreferences sharedpreferences;
    int ClientId = 0;

    ArrayAdapter<String> salutationadaptor,parentadaptor;

    public List<BasicModel> currencytype = new ArrayList<>();
    public List<BasicModel> modetype = new ArrayList<>();
    public List<BasicModel> paymenttype = new ArrayList<>();

    public DatePickerDialog picker;

    HashMap<String,String> map = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_customer);

        Initialize_ids();

        dated.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    final Calendar cldr = Calendar.getInstance();
                    int day = cldr.get(Calendar.DAY_OF_MONTH);
                    int month = cldr.get(Calendar.MONTH);
                    int year = cldr.get(Calendar.YEAR);
                    picker = new DatePickerDialog(NewCustomer.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    Calendar calendar = Calendar.getInstance();
                                    dated.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                    dated.clearFocus();
                                }
                            }, year, month, day);
                    picker.setButton(DialogInterface.BUTTON_NEGATIVE, "", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == DialogInterface.BUTTON_NEGATIVE) {
                                dated.clearFocus();
                            }
                        }
                    });
                    picker.show();
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Loader.setVisibility(View.VISIBLE);
                isonline = isOnline();
                if (isonline)
                {
                    CheckAll();
                }
                else
                {
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                        alertDialog.setTitle("KGC PROPERTIES");
                        alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                        alertDialog.setIcon(R.drawable.loginlogo);
                        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        alertDialog.show();
                    } catch (Exception e) {
                        Log.d("Haseeb", "Show Dialog: " + e.getMessage());
                    }
                }
                if (name.equals(""))
                {
                    Name.setError("Invalid Name");
                    Loader.setVisibility(View.GONE);
                }
                else if (phone.equals(""))
                {
                    Phone.setError("Invalid PhoneNo");
                    Loader.setVisibility(View.GONE);
                }
                else if (cnic.equals("") || cnic.length() < 13)
                {
                    Cnic.setError("Invalid CNIC");
                    Loader.setVisibility(View.GONE);
                }
                else if (currencttext.equals(""))
                {
                    CurrencyText.setError("Invalid Currency");
                    Loader.setVisibility(View.GONE);
                }
                else if ( Amount.equals("") || Amount.contains(".,"))
                {
                    amount.setError("Invalid Amount");
                    Loader.setVisibility(View.GONE);
                }
                else if (typeofpayment.equals(""))
                {
                    TypeOfPayment.setError("Invalid Payment Type");
                    Loader.setVisibility(View.GONE);
                }
                else if (Dated.equals(""))
                {
                    dated.setError("Invalid Date");
                    Loader.setVisibility(View.GONE);
                }
                else if (Plot.equals(""))
                {
                    plot.setError("Invalid Plot/Unit No");
                    Loader.setVisibility(View.GONE);
                }
                else if (Installment.equals(""))
                {
                    installment.setError("Invalid Installment No");
                    Loader.setVisibility(View.GONE);
                }
                else if (through.equals(""))
                {
                    Through.setError("Invalid Medium");
                    Loader.setVisibility(View.GONE);
                }
                else
                {
                    isonline = isOnline();
                    if (isonline)
                    {
                        JSONObject json = Params();
                        SaveReciept(json);
                    }
                    else
                    {
                        try {
                            AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();

                            alertDialog.setTitle("KGC PROPERTIES");
                            alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                            alertDialog.setIcon(R.drawable.loginlogo);
                            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });

                            alertDialog.show();
                        } catch (Exception e) {
                            Log.d("Haseeb", "Show Dialog: " + e.getMessage());
                        }
                    }
                }

            }
        });

        salutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                SalutationDescription = salutation.getItemAtPosition(position).toString();
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        parent.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                RelationDescription = parent.getItemAtPosition(position).toString();
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        Currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                String CurrencyTypeDescription = ((BasicModel) Currency.getSelectedItem ()).getdescription();
                if (CurrencyTypeDescription.equals("AnyOther"))
                {
                    CurrencyText.setText("");
                }
                else
                {
                    CurrencyText.setText(CurrencyTypeDescription);
                }

            }
            public void onNothingSelected(AdapterView<?> arg0) {
                String CurrencyTypeDescription = ((BasicModel) Currency.getSelectedItem ()).getdescription();
                CurrencyText.setText(CurrencyTypeDescription);
            }
        });

        Mode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                String PaymentTypeDescription = ((BasicModel) Mode.getSelectedItem ()).getdescription();
                if (PaymentTypeDescription.equals("Cash"))
                {
                    TypeOfPayment.setText(PaymentTypeDescription);
                    dated.setText(GetDateTime());
                    //dated.setFocusable(false);
                }
                else
                {
                    //dated.setFocusable(true);
                    TypeOfPayment.setText("");
                    dated.setText("");

                }

            }
            public void onNothingSelected(AdapterView<?> arg0) {
                String CurrencyTypeDescription = ((BasicModel) Mode.getSelectedItem ()).getdescription();
                TypeOfPayment.setText(CurrencyTypeDescription);
            }
        });

        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id) {
                String TYPE = ((BasicModel) type.getSelectedItem ()).getdescription();
                if (TYPE.equals("Down Payment"))
                {
                    installment.setText("Down Payment");
                }
                else if (TYPE.equals("Token"))
                {
                    installment.setText("Token");
                }
                else if (TYPE.equals("Balloon"))
                {
                    installment.setText("Balloon Payment");
                }
                else if (TYPE.equals("Work Based"))
                {
                    installment.setText("Work Based");
                }
                else if (TYPE.equals("Development"))
                {
                    installment.setText("Developement Charges");
                }
                else
                {
                    installment.setText("");
                }
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

    }

    private boolean CheckAll(){
        currencyid = ((BasicModel) Currency.getSelectedItem ()).getid();
        modeid = ((BasicModel) Mode.getSelectedItem ()).getid();
        typeid = ((BasicModel) type.getSelectedItem ()).getid();

        fileno = file.getText().toString().trim();
        name = Name.getText().toString().trim();
        parentname = ParentName.getText().toString().trim();
        email = Email.getText().toString().trim();
        currencttext = CurrencyText.getText().toString().trim();
        Dated = dated.getText().toString().trim();
        Plot = plot.getText().toString().trim();
        through = Through.getText().toString().trim();
        Remarks = remarks.getText().toString().trim();

        phone = Phone.getText().toString().trim();
        cnic = Cnic.getText().toString().trim();
        passport = Passport.getText().toString().trim();
        Amount = amount.getText().toString().trim();
        typeofpayment = TypeOfPayment.getText().toString().trim();
        Installment = installment.getText().toString().trim();
        return true;
    }

    private JSONObject Params()
    {
        JSONObject param = new JSONObject();
        try {
            if (ClientId != 0) { param.put("ClientId",ClientId); }
            else { }
            param.put("salutation",SalutationDescription);
            param.put("ClientName",name);
            param.put("Relation",RelationDescription);
            param.put("FamailyName",parentname);
            param.put("cnic",cnic);
            param.put("PassportNo",passport);
            param.put("EmailId",email);
            param.put("Status_id",101);
            param.put("Date",Dated);
            param.put("PhoneNumber",phone);
            param.put("CurrencyTypeId",currencyid);
            param.put("CurrencyTypeDescription",currencttext);
            param.put("Amount",Integer.parseInt(Amount));
            param.put("PaymentThroughId",modeid);
            param.put("PaymentThroughDescription",typeofpayment);
            param.put("Plot_UnitNo",Plot);
            param.put("InstallmentNo",Installment);
            param.put("Through",through);
            param.put("PaymentTypeid",typeid);
            param.put("Agentid",sharedpreferences.getInt("Dashboarduserid",0));
            param.put("Confirmation_Status",103);
            param.put("Remarks",Remarks);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;
    }

    //************************************************************************************************//
    private void Initialize_ids()
    {
        Loader = findViewById(R.id.loader);
        Loader.setVisibility(View.VISIBLE);

        file = findViewById(R.id.file);
        Name = findViewById(R.id.Name);
        ParentName = findViewById(R.id.ParentName);
        Phone = findViewById(R.id.Phone);
        Email = findViewById(R.id.Email);
        Cnic = findViewById(R.id.Cnic);
        Passport = findViewById(R.id.Passport);
        CurrencyText = findViewById(R.id.CurrencyText);
        amount = findViewById(R.id.amount);
        TypeOfPayment = findViewById(R.id.TypeOfPayment);
        plot = findViewById(R.id.plot);
        dated = findViewById(R.id.dated);
        installment = findViewById(R.id.installment);
        Through = findViewById(R.id.Through);
        remarks = findViewById(R.id.remarks);

        btnSave = findViewById(R.id.btnSave);

        salutation= findViewById(R.id.salutation);
        parent= findViewById(R.id.parent);
        Currency= findViewById(R.id.Currency);
        type= findViewById(R.id.type);
        Mode= findViewById(R.id.Mode);

        salutationadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.NameStarting));
        salutationadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        salutation.setAdapter(salutationadaptor);

        parentadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.ParentStarting));
        parentadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        parent.setAdapter(parentadaptor);

        try {
            map = (HashMap<String, String>) getIntent().getSerializableExtra("details");
            if (map.size() > 0)
            {
                String SALUTATION = map.get("salutation");
                String RELATION = map.get("Relation");
                if (SALUTATION.equals("Mr"))
                {
                    salutationadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.NameStarting));
                    salutationadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    salutation.setAdapter(salutationadaptor);
                    salutation.setSelection(0);
                    salutationadaptor.notifyDataSetChanged();
                }
                else if (SALUTATION.equals("Ms"))
                {
                    salutationadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.NameStarting));
                    salutationadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    salutation.setAdapter(salutationadaptor);
                    salutation.setSelection(1);
                    salutationadaptor.notifyDataSetChanged();
                }
                else if (SALUTATION.equals("Mrs"))
                {
                    salutationadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.NameStarting));
                    salutationadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    salutation.setAdapter(salutationadaptor);
                    salutation.setSelection(2);
                    salutationadaptor.notifyDataSetChanged();
                }
                else {
                    salutationadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.NameStarting));
                    salutationadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    salutation.setAdapter(salutationadaptor);
                }

                if (RELATION.equals("S/o"))
                {
                    parentadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.ParentStarting));
                    parentadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    parent.setAdapter(parentadaptor);
                    parent.setSelection(0);
                    parentadaptor.notifyDataSetChanged();
                }
                else if (RELATION.equals("D/o"))
                {
                    parentadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.ParentStarting));
                    parentadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    parent.setAdapter(parentadaptor);
                    parent.setSelection(1);
                    parentadaptor.notifyDataSetChanged();
                }
                else if (RELATION.equals("W/o"))
                {
                    parentadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.ParentStarting));
                    parentadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    parent.setAdapter(parentadaptor);
                    parent.setSelection(2);
                    parentadaptor.notifyDataSetChanged();
                }
                else {
                    parentadaptor= new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.ParentStarting));
                    parentadaptor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    parent.setAdapter(parentadaptor);
                }

                ClientId = Integer.parseInt(map.get("ClientId"));
                Name.setText(map.get("ClientName"));
                ParentName.setText(map.get("FamailyName"));
                Phone.setText(map.get("PhoneNumber"));
                Cnic.setText(map.get("CNIC"));
                Email.setText(map.get("EmailId"));
                Passport.setText(map.get("PassportNo"));

                if (map.get("FileNumber").equals("N/A") || map.get("FileNumber").equals(""))
                {
                    //Donothing
                }
                else
                {
                    file.setText(map.get("FileNumber"));
                }
            }
            else
            {
                //Do nothing
            }
        } catch (Exception e) {

        }


        //***********************************************************************************//
        isonline = isOnline();
        if (isonline)
        {
            GetCurrencyType();
            GetModeType();
            GetPaymentType();
        }
        else
        {
            try {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();

                alertDialog.setTitle("KGC PROPERTIES");
                alertDialog.setMessage("Internet not available, Cross check your internet connectivity and try again");
                alertDialog.setIcon(R.drawable.loginlogo);
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                alertDialog.show();
            } catch (Exception e) {
                Log.d("Haseeb", "Show Dialog: " + e.getMessage());
            }
        }

        //****************************************************************************************************************************************//
    }

    private void GetCurrencyType()
    {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url + "CurrencyType", null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CurrencyType: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                JSONArray RESPONSE = response.getJSONArray("response");
                                for (int i = 0 ; i < RESPONSE.length(); i++)
                                {
                                    JSONObject json = RESPONSE.getJSONObject(i);
                                    int id = json.getInt("CurrencyTypeId");
                                    String description = json.getString("CurrencyTypeName");

                                    BasicModel basicModel = new BasicModel();
                                    basicModel.setid(id);
                                    basicModel.setdescription(description);
                                    currencytype.add(basicModel);
                                }

                                ArrayAdapter<BasicModel> dataAdapter = new ArrayAdapter<BasicModel>(getApplicationContext(),
                                        android.R.layout.simple_spinner_dropdown_item,currencytype);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                Currency.setAdapter(dataAdapter);

                                //Loader.setVisibility(View.GONE);

                            }
                            else{
                                //progressBar.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Toast.makeText(NewCustomer.this, "ERROR: " + e.toString(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CurrencyType: " + error.toString());
                        //progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(NewCustomer.this);
        requestQueue.add(jsonObjectRequest);
    }
    //************************************************************************************************//
    private void GetPaymentType()
    {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url + "PaymentType", null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","PaymentType: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                JSONArray RESPONSE = response.getJSONArray("response");
                                for (int i = 0 ; i < RESPONSE.length(); i++)
                                {
                                    JSONObject json = RESPONSE.getJSONObject(i);
                                    int id = json.getInt("PaymentTypeId");
                                    String description = json.getString("PaymentTypeName");

                                    BasicModel basicModel = new BasicModel();
                                    basicModel.setid(id);
                                    basicModel.setdescription(description);
                                    paymenttype.add(basicModel);
                                }

                                ArrayAdapter<BasicModel> dataAdapter = new ArrayAdapter<BasicModel>(getApplicationContext(),
                                        android.R.layout.simple_spinner_dropdown_item,paymenttype);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                type.setAdapter(dataAdapter);
                                Loader.setVisibility(View.GONE);

                            }
                            else{
                                //progressBar.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","PaymentType: " + error.toString());
                        //progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(NewCustomer.this);
        requestQueue.add(jsonObjectRequest);
    }
    //************************************************************************************************//
    private void GetModeType()
    {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url + "PaymentThrough", null,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","PaymentThrough: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                JSONArray RESPONSE = response.getJSONArray("response");
                                for (int i = 0 ; i < RESPONSE.length(); i++)
                                {
                                    JSONObject json = RESPONSE.getJSONObject(i);
                                    int id = json.getInt("PaymentThroughId");
                                    String description = json.getString("PaymentthroughName");

                                    BasicModel basicModel = new BasicModel();
                                    basicModel.setid(id);
                                    basicModel.setdescription(description);
                                    modetype.add(basicModel);
                                }

                                ArrayAdapter<BasicModel> dataAdapter = new ArrayAdapter<BasicModel>(getApplicationContext(),
                                        android.R.layout.simple_spinner_dropdown_item,modetype);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                Mode.setAdapter(dataAdapter);

                                //Loader.setVisibility(View.GONE);

                            }
                            else{
                                //progressBar.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","PaymentThrough: " + error.toString());
                        //progressBar.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(NewCustomer.this);
        requestQueue.add(jsonObjectRequest);
    }

    private String GetDateTime()
    {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String dateToStr = format.format(today);
        return dateToStr;
    }

    private void SaveReciept(JSONObject param)
    {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "CreateReceipt", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CreateReceipt: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                JSONObject json = response.getJSONObject("response");
                                id = json.getInt("id");
                                if (fileno.equals(""))
                                {
                                    Notify();
                                    Loader.setVisibility(View.GONE);
                                }
                                else
                                {
                                    UpdatePaymentFileNumber();
                                    Loader.setVisibility(View.GONE);
                                }

                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                                NewCustomer.this.startActivity (new Intent(NewCustomer.this, CustomersList.class));
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            Toast.makeText(NewCustomer.this, e.toString(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CreateReceipt: " + error.toString());
                        Toast.makeText(NewCustomer.this, error.toString(), Toast.LENGTH_LONG).show();
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(NewCustomer.this);
        requestQueue.add(jsonObjectRequest);
    }

    private void Notify()
    {
        JSONObject param = new JSONObject();
        try {
            param.put("title", "New Entry Recieved");
            param.put("message", name + "'s data waiting for approval");
            param.put("fcmtoken", "/topics/Account");
            param.put("Agentid", sharedpreferences.getInt("Dashboarduserid", 0));
            param.put("PaymentReceiptNo", id);
            param.put("Confirmation_Status", 103);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "UpdateNotify", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CurrencyType: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "Success", Toast.LENGTH_LONG).show();
                                NewCustomer.this.startActivity (new Intent(NewCustomer.this, CustomersList.class));
                                finish();

                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CurrencyType: " + error.toString());
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(NewCustomer.this);
        requestQueue.add(jsonObjectRequest);
    }

    private void UpdatePaymentFileNumber()
    {
        JSONObject param = new JSONObject();
        try {
            param.put("Filenumber",fileno);
            param.put("PaymentReceiptNo",id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url + "UpdatePaymentFileNumber", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CreateReceipt: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                Notify();
                                Loader.setVisibility(View.GONE);
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(NewCustomer.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            Toast.makeText(NewCustomer.this, e.toString(), Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CreateReceipt: " + error.toString());
                        Toast.makeText(NewCustomer.this, error.toString(), Toast.LENGTH_LONG).show();
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(NewCustomer.this);
        requestQueue.add(jsonObjectRequest);
    }
    //************************************************************************************************//
    public void onback()
    {
        dated.clearFocus();
    }


    public boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }
}