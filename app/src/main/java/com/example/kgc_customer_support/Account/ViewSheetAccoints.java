package com.example.kgc_customer_support.Account;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.kgc_customer_support.Login;
import com.example.kgc_customer_support.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.example.kgc_customer_support.Login.mypreference;
import static com.example.kgc_customer_support.Login.url;

public class ViewSheetAccoints extends AppCompatActivity {

    LinearLayout FILENO,FATHERNAME,EMAIL,PASSPORT;
    TextView RecieptNo,GeneratedDate,FileNo,Name,FatherName,ContactNo,Email,Cnic,Passport,Amount,ModeOfPayment,ModeDescription,Dated,PlotNo,PaymentType,DrawnThrough,Remarks;
    SharedPreferences sharedpreferences;
    HashMap<String,String> MAP = new HashMap<>();
    ProgressBar Loader;
    Button btnAccept,btnReject;
    TextView EDIT;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_sheet_accoints);

        Initialize_ids();

        EDIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewSheetAccoints.this, ViewDetailAccounts.class);
                intent.putExtra("details",MAP);
                startActivity(intent);
                finish();
            }
        });
    }

    private void Initialize_ids()
    {
        sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
        Loader = findViewById(R.id.loader);
        Loader.setVisibility(View.VISIBLE);

        RecieptNo = findViewById(R.id.RecieptNo);
        GeneratedDate = findViewById(R.id.GeneratedDate);
        FileNo = findViewById(R.id.FileNo);
        Name = findViewById(R.id.Name);
        FatherName = findViewById(R.id.FatherName);
        ContactNo = findViewById(R.id.ContactNo);
        Email = findViewById(R.id.Email);
        Cnic = findViewById(R.id.Cnic);
        Passport = findViewById(R.id.Passport);
        Amount = findViewById(R.id.Amount);
        ModeOfPayment = findViewById(R.id.ModeOfPayment);
        ModeDescription = findViewById(R.id.ModeDescription);
        Dated = findViewById(R.id.Dated);
        PlotNo = findViewById(R.id.PlotNo);
        PaymentType = findViewById(R.id.PaymentType);
        DrawnThrough = findViewById(R.id.DrawnThrough);
        Remarks = findViewById(R.id.Remarks);

        FILENO = findViewById(R.id.FILENO);
        FATHERNAME = findViewById(R.id.FATHERNAME);
        EMAIL= findViewById(R.id.EMAIL);
        PASSPORT= findViewById(R.id.PASSPORT);

        btnAccept = findViewById(R.id.btnApprove);
        btnReject = findViewById(R.id.btnReject);
        EDIT = findViewById(R.id.EDIT);

        SetData();
    }

    private void SetData()
    {
        MAP = (HashMap<String, String>) getIntent().getSerializableExtra("details");
        RecieptNo.setText(MAP.get("PaymentReceiptNo"));
        GeneratedDate.setText(MAP.get("Datetime"));
        Name.setText(MAP.get("salutation") + " " + MAP.get("ClientName"));
        ContactNo.setText(MAP.get("PhoneNumber"));
        Cnic.setText(MAP.get("CNIC"));
        Amount.setText(MAP.get("Amount") + " " + MAP.get("CurrencyTypeDescription"));
        ModeOfPayment.setText(MAP.get("PaymentthroughName"));
        ModeDescription.setText(MAP.get("PaymentThroughDescription"));
        Dated.setText(MAP.get("Dated"));
        PlotNo.setText(MAP.get("Plot_UnitNo"));
        PaymentType.setText(MAP.get("InstallmentNo"));
        DrawnThrough.setText(MAP.get("Through"));
        Remarks.setText(MAP.get("Remarks"));

        FileNo.setText(MAP.get("FileNumber"));
        FatherName.setText(MAP.get("Relation") +" "+ MAP.get("FamailyName"));
        Passport.setText(MAP.get("PassportNo"));
        Email.setText(MAP.get("EmailId"));

        String fileno = MAP.get("FileNumber");
        String fathername = MAP.get("FamailyName");
        String passport = MAP.get("PassportNo");
        String email = MAP.get("EmailId");

        if (fileno.equals("N/A") || fileno.equals(""))
        {
            FILENO.setVisibility(View.GONE);
        }
        if (fathername.equals("N/A") || fathername.equals(""))
        {
            FATHERNAME.setVisibility(View.GONE);
        }
        if (passport.equals("N/A") || passport.equals(""))
        {
            PASSPORT.setVisibility(View.GONE);
        }
        if (email.equals("N/A") || email.equals(""))
        {
            EMAIL.setVisibility(View.GONE);
        }

        String BUTTONTEXT = MAP.get("status_name");
        if (BUTTONTEXT.equals("Closed") || BUTTONTEXT.equals("Approved") || BUTTONTEXT.equals("Cancelled"))
        {
            btnAccept.setVisibility(View.GONE);
            btnReject.setVisibility(View.GONE);
            EDIT.setVisibility(View.GONE);
            Loader.setVisibility(View.GONE);
        }
        else if (BUTTONTEXT.equals("Reject"))
        {
            btnAccept.setVisibility(View.VISIBLE);
            EDIT.setVisibility(View.VISIBLE);
            btnReject.setVisibility(View.GONE);
            Loader.setVisibility(View.GONE);
        }
        else
        {
            btnAccept.setVisibility(View.VISIBLE);
            btnReject.setVisibility(View.VISIBLE);
            EDIT.setVisibility(View.VISIBLE);
            Loader.setVisibility(View.GONE);
        }

        Loader.setVisibility(View.GONE);
    }

    public void Accept(View view)
    {
        Loader.setVisibility(View.VISIBLE);
        UpdateStatus(104);
    }

    public void Reject(View view)
    {
        Loader.setVisibility(View.VISIBLE);
        UpdateStatus(106);
    }

    private void UpdateStatus(int status)
    {
        JSONObject param = new JSONObject();
        try {
            param.put("PaymentReceiptNo",Integer.parseInt(MAP.get("PaymentReceiptNo")));
            param.put("Confirmation_Status",status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, url + "UpdatePaymentStatus", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","UpdatePaymentStatus: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                Notify(status);
                            }
                            else{
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(ViewSheetAccoints.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","UpdatePaymentStatus: " + error.toString());
                        Loader.setVisibility(View.GONE);;
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(ViewSheetAccoints.this);
        requestQueue.add(jsonObjectRequest);
    }

    private void Notify(int status)
    {
        Loader.setVisibility(View.VISIBLE);
        JSONObject param = new JSONObject();
        if (status == 104)
        {
            try {
                param.put("title", "New Payment Approved");
                param.put("message", MAP.get("ClientName") + "'s data waiting for confirmation");
                param.put("fcmtoken", "/topics/Documentation");
                param.put("Agentid", sharedpreferences.getInt("Dashboarduserid", 0));
                param.put("PaymentReceiptNo", Integer.parseInt(MAP.get("PaymentReceiptNo")));
                param.put("Confirmation_Status", status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                param.put("title", "Payment Rejected");
                param.put("message", MAP.get("ClientName") + "'s data rejected");
                param.put("fcmtoken", "/topics/DataEntry");
                param.put("Agentid", sharedpreferences.getInt("Dashboarduserid", 0));
                param.put("PaymentReceiptNo", Integer.parseInt(MAP.get("PaymentReceiptNo")));
                param.put("Confirmation_Status", status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url + "UpdateNotify", param,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("KGCRESPONSE","CurrencyType: " + response.toString());
                        try {
                            if (response.getString("status").equals("true") && response.getString("message").equals("Sucessfull"))
                            {
                                Loader.setVisibility(View.GONE);
                                Toast.makeText(ViewSheetAccoints.this, "Success", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(ViewSheetAccoints.this, CustomerAccountsHistory.class);
                                int clientid = Integer.parseInt(MAP.get("ClientId"));
                                intent.putExtra("clientid",clientid);
                                startActivity(intent);
                                finish();
                                Loader.setVisibility(View.GONE);

                            }
                            else{
                                Loader.setVisibility(View.GONE);;
                                Toast.makeText(ViewSheetAccoints.this, "ERROR: " + response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            Loader.setVisibility(View.GONE);
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("KGCERROR","CurrencyType: " + error.toString());
                        Loader.setVisibility(View.GONE);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> param = new HashMap<String, String>();
                sharedpreferences = getSharedPreferences(mypreference, Context.MODE_PRIVATE);
                param.put("Authorization", "bearar " + sharedpreferences.getString("token", ""));
                return param;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(ViewSheetAccoints.this);
        requestQueue.add(jsonObjectRequest);
    }
}